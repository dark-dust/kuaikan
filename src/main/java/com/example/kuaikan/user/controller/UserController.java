package com.example.kuaikan.user.controller;

import com.example.kuaikan.user.bean.User;
import com.example.kuaikan.user.service.UserService;
import com.example.kuaikan.util.SendMail;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 用户控制器
 */

@RequestMapping("/user")
@RestController
public class UserController {
    @Resource
    UserService userService;

    //登录
    @RequestMapping("/login")
    public User Login(User user){
        System.out.println(user);
        return userService.login(user);
    }

    //发送验证吗
    @GetMapping("/verification")
    public String verification(@RequestParam String email) throws Exception {
        String verification= SendMail.getVCode();
        SendMail.sendMail(email,verification); //发送邮件
        return verification;
    }

    //注册
    @PostMapping("/register")
    public User register(@RequestBody User user) throws Exception {
        userService.add(user);
        return user;
    }

    /*
        用id获取用户信息
    */
    @GetMapping("/queryBatch")
    public User queryBatch(User user){
        return userService.query(user);
    }

    /*
     * 修改用户信息
     */
    @PostMapping("/setuser")
    public boolean setuser(@RequestBody User user){
        System.out.println(user);
        return userService.update(user);
    }
}
