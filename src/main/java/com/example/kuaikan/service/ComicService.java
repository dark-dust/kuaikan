package com.example.kuaikan.service;

import com.example.kuaikan.entity.Comic;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ComicService {
    List query();
//    查询5条数据
    List five();
//    查询10条数据
    List ten();
//    查询排名最高的10条
    List max();
//    查询排名前三的
    List maxthree();
//    根据id查详情
    List iddetails(@Param("uid") int uid);
//    增加数据
boolean addquery(Comic ob);
//   查询id
    List idx(@Param("uid") int uid);

//    根据id查询章节数量
    int idse(@Param("uid") int uid);
}

