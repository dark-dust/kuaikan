package com.example.kuaikan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class KuaikanApplication {

    public static void main(String[] args) {
        SpringApplication.run(KuaikanApplication.class, args);
    }

}
