package com.example.kuaikan;

import com.example.kuaikan.entity.Section;
import com.example.kuaikan.ranking.dao.RankingDao;
import com.example.kuaikan.ranking.entity.Comics;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootTest
public class RinkingTest {

    @Resource
    RankingDao rankingDao;

    /**
     * 查询所有漫画
     */
    @Test
    void listComic(){
        List item = rankingDao.listComic();
        List up = rankingDao.listSection();
        List list = new ArrayList();
        List list2 = new ArrayList();
        while (list.size() < 14){
            for(int i=0; i<item.size();i++){
                Comics cc = (Comics)item.get(i);
                Map map = new HashMap();
                map.put("id",cc.getId());
                map.put("labels", new String[]{cc.getTag()});
                map.put("img",cc.getImg_cover());
                map.put("name", cc.getName());
                map.put("author",cc.getUsername());
                map.put("description",cc.getDetails());
                map.put("recommend",cc.getRecommend());
                map.put("popularity",cc.getPopularity());
                map.put("update", "");
                up.forEach(v -> {
                    Section se = (Section)v;
                    if(se.getComic_id()==cc.getId()){
                        map.put("update", se.getNickname());
                    }
                });

                list2.add(map);
                if(list2.size()==10){
                    list.add(list2);
                    list2 = new ArrayList();
                }
            }
        }
        AtomicInteger i= new AtomicInteger(1);
        list.forEach(item2 -> System.out.println(i.getAndIncrement() +"> "+item2));
    }
}
