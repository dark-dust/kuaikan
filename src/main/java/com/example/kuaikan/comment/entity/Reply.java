package com.example.kuaikan.comment.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reply {
    int id;
    String text;
    int userid;
    int worldid;
    Date date;
    String username;

    @Override
    public String toString() {
        return "\nReply{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", userid=" + userid +
                ", worldid=" + worldid +
                ", date=" + date +
                ", username='" + username + '\'' +
                '}';
    }
}
