package com.example.kuaikan.classify.controller;

import com.example.kuaikan.classify.dao.ClassifyDao;
import com.example.kuaikan.classify.entity.Comic2;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class ComicController {
    @Autowired
    ClassifyDao classifyDao;
    @GetMapping("/Comic")
    public @ResponseBody List<Map> comic(ModelMap map,String pg) {
        //        分类标签
        Page<Comic2> page;
        List listTag=classifyDao.getComicTag();
        System.out.println(pg);
        if (pg == null) {
            System.out.println("111111");
               page = new Page<>( 1, 12);
        } else {
               page = new Page<>( Integer.parseInt(pg), 12);
        }
//        漫画数据
        List list = classifyDao.getComic(page);
        System.out.println("page"+page);
        List<Map> listMap = new ArrayList<Map>();
        System.out.println(list);
        map.put("tag",listTag);
        map.put("list", list);
        map.put("p", page);
        listMap.add(map);
        return listMap;
    }
    @GetMapping("/Comicbq")
    public @ResponseBody List<Map> comicbq(ModelMap map,String pg,String tag) {
        System.out.println(tag);
        Page<Comic2> page;
        if (pg == null) {
            System.out.println("111111");
            page = new Page<>( 1, 12);
        } else {
            page = new Page<>( Integer.parseInt(pg), 12);
        }
        List list = classifyDao.getBqComic(page,tag);
        List<Map> listMap = new ArrayList<Map>();
        listMap.add(map);
        map.put("list", list);
        map.put("p", page);
        return listMap;
    }
    //   人气值查询
    @GetMapping("/ComicHot")
    public @ResponseBody List<Map> comichot(ModelMap map,String pg,String tag) {
        Page<Comic2> page;
        if (pg == null) {
            System.out.println("111111");
            page = new Page<>( 1, 12);
        } else {
            page = new Page<>( Integer.parseInt(pg), 12);
        }
        List list = classifyDao.getComicHot(page,tag);
        List<Map> listMap = new ArrayList<Map>();
        listMap.add(map);
        map.put("list", list);
        map.put("p", page);
        return listMap;
    }
    //    后台根据用户id查询漫画
    @GetMapping("/ComicUser")
    public @ResponseBody
    List<Map> comicUser(ModelMap map, String pg, int author) {
        Page<Comic2> page;
        if (pg == null) {
            page = new Page<>(1, 6);
        } else {
            page = new Page<>(Integer.parseInt(pg), 6);
        }
        List list = classifyDao.getComicUser(page, author);
        List<Map> listMap = new ArrayList<Map>();
        listMap.add(map);
        map.put("list", list);
        map.put("p", page);
        return listMap;
    }
    //    投稿漫画
    @GetMapping("/ComicTouGao")
    public @ResponseBody
    List<Map> comicTougao(ModelMap map,  int author) {
        List list = classifyDao.getComicTouGao( author);
        List<Map> listMap = new ArrayList<Map>();
        listMap.add(map);
        map.put("list", list);
        return listMap;
    }
    //    投稿章节
    @GetMapping("/ComicZj")
    public @ResponseBody
    List<Map> comicZj(ModelMap map,  int id) {
        List list = classifyDao.getComicSection( id);

        List<Map> listMap = new ArrayList<Map>();
        listMap.add(map);
        map.put("list", list);
        return listMap;
    }
//    投稿查询
    @GetMapping("/TouGaoSecrch")
        public @ResponseBody
            List<Map> TouGaoSec(ModelMap map,  int author, String name) {
            List list = classifyDao.getTouGaoSearch(author,name);
            List<Map> listMap = new ArrayList<Map>();
            listMap.add(map);
            map.put("list", list);
            return listMap;
        }
//    漫画删除
    @GetMapping("/DelTouGao")
        public @ResponseBody
        boolean DelTouGao( int id) {
            int num = classifyDao.getDelTouGao(id);
            if(num==1){
                return true;
            }else{
                return false;
            }
}//    漫画人气值修改
    @GetMapping("/getUpdDetails")
        public @ResponseBody
            boolean getUpdDetails(int popularity,int id) {
            int num = classifyDao.getUpdDetails(popularity,id);
            if(num==1){
                return true;
                }else{
                return false;
            }
            }
        }
