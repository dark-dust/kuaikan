package com.example.kuaikan.service.impl;


import com.example.kuaikan.dao.ComicDao;
import com.example.kuaikan.dao.SectionDao;
import com.example.kuaikan.entity.Comic;
import com.example.kuaikan.entity.Section;
import com.example.kuaikan.service.ComicService;
import com.example.kuaikan.service.SectionService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComicServiceImpl implements ComicService {
    @Autowired
    ComicDao comicDao;
    @Override
    public List query() {
        List n =comicDao.query();
        return n;
    }

    @Override
    public List five() {
        List m = comicDao.five();
        return m;
    }

    @Override
    public List ten() {
        List m = comicDao.ten();
        return m;
    }

    @Override
    public List max() {
        List m = comicDao.max();
        return m;
    }

    @Override
    public List maxthree() {
        List m = comicDao.maxthree();
        return m;
    }

    @Override
    public List iddetails(int uid) {
        List m = comicDao.iddetails(uid);
        return m;
    }

    @Override
    public boolean addquery(Comic ob) {
        boolean m = comicDao.addquery(ob);
        return m;
    }

    @Override
    public List idx(int uid) {
        List m = comicDao.idx(uid);
        return m;
    }

    @Override
    public int idse(int uid) {
        int n = comicDao.idse(uid);
        return n;
    }
}
