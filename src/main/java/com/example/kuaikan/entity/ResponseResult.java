package com.example.kuaikan.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ResponseResult {
        public int code;
    public String msg;
    public Map<String,List<String>> map=new HashMap<>();

        public ResponseResult() {
        }


        public ResponseResult(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        @Override
        public String toString() {
            return "JsonResult{" +
                    "code=" + code +
                    ", msg='" + msg + '\'' +
                    ", map=" + map +
                    '}';
        }

        public ResponseResult(int code, String msg, Map<String, List<String>> map) {
            this.code = code;
            this.msg = msg;
            this.map = map;
        }
    }

