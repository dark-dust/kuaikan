package com.example.kuaikan.controller;

import com.alibaba.fastjson.JSON;

import com.example.kuaikan.entity.Comic;
import com.example.kuaikan.entity.JsonResult;
import com.example.kuaikan.entity.ResponseResult;
import com.example.kuaikan.entity.Section;
import com.example.kuaikan.service.ComicService;
import com.example.kuaikan.service.SectionService;

import com.example.kuaikan.util.ImageUtils;
import com.example.kuaikan.util.ImgUpload;
import com.google.gson.JsonObject;
import lombok.Data;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;


/*
* 上传文件控制器
* */
@RestController
@RequestMapping("/api")

public class ImageController {

    @Autowired
    private ImageUtils imageUtils;
    @Autowired
    private SectionService sectionService;
    @Autowired
    private ComicService comicService;
    int n;

//    上传一张图片
    @ResponseBody
    @RequestMapping("/image/day")
    public String uploadImage(@RequestParam(value = "file", required = false) MultipartFile[] multipartFile){
        System.out.println(multipartFile);
        Map<String, List<String>> uploadImagesUrl = imageUtils.uploadImages(multipartFile);
        System.out.println(uploadImagesUrl);
        String o="";
        for(Object m:uploadImagesUrl.values()){
//            System.out.println(m);
            o = m.toString();
        }
        System.out.println(o);
        return o;
    }

//    上传多张图片
    @PostMapping("/image/upload")
    public ResponseResult uploadImage(@RequestParam(value = "file", required = false) MultipartFile[] multipartFile, HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        if (ObjectUtils.isEmpty(multipartFile)) {
            return new ResponseResult(500, "请选择图片");
        }
        Map<String, List<String>> uploadImagesUrl = imageUtils.uploadImages(multipartFile);
        List<String> obje = new ArrayList<>();

        List<String> lists = (List<String>) httpSession.getAttribute("us");
        if (lists != null) {
            obje = lists;
        }
        for (Map.Entry<String, List<String>> entry : uploadImagesUrl.entrySet()) {
            //key = entry.getKey()   Value=entry.getValue()
            System.out.println(entry.getValue().size());
            if (entry.getValue().size() == 1) {
                String obj = entry.getValue().get(0);
                obje.add(obje.size(), obj);
                httpSession.setAttribute("us", obje);
            } else {
                List arr = entry.getValue();
                httpSession.setAttribute("us", arr);
            }
        }
        Object ob = httpSession.getAttribute("us");
        String arr = "";
        List list = JSON.parseArray(JSON.toJSONString(ob));
        for (int i = 0; i < list.size(); i++) {
            arr += list.get(i) + " ";
        }
        System.out.println(arr);
        if (arr != null) {
            Section obj = new Section(244, arr, 1, "123123", 1);
            int po = sectionService.adddata(obj);
            System.out.println(po);
        } else {
            System.out.println("报错了");
        }
        return new ResponseResult(200, "上传成功", uploadImagesUrl);
    }

    @RequestMapping("/query")
    @ResponseBody
    public JsonResult tianjia() {
        List list = comicService.query();
        Map<String,Object> map = new HashMap<>();
        map.put("fo",list);

        System.out.println(list);
        JsonResult jsonResult = new JsonResult(200,"添加成功",map);
        return jsonResult;
    }

    //    封面上传
    @RequestMapping("/addtian")
    @ResponseBody
    public int add(@RequestBody Comic comic) {
        System.out.println(comic);
//        Comic c = new Comic(1, "aaa", "awd", "123123",0,0,0,"1 2323");
//        Comic c = new Comic(comic);
        boolean list = comicService.addquery(comic);
        n = comic.getId();
        System.out.println(n);
//        Map<String,Object> map = new HashMap<>();
//        map.put("fo",list);
////        System.out.println(list);
//        JsonResult jsonResult = new JsonResult(200,"添加成功",map);
        return n;
    }

    @RequestMapping("/five")
    @ResponseBody
    public JsonResult fi() {
        List list = comicService.five();
        Map<String,Object> map = new HashMap<>();
        map.put("fo",list);
        System.out.println(list);
        JsonResult jsonResult = new JsonResult(200,"查询成功",map);
        return jsonResult;
    }

    @RequestMapping("/ten")
    @ResponseBody
    public JsonResult fp() {
        List list = comicService.ten();
        Map<String,Object> map = new HashMap<>();
        map.put("fo",list);
        System.out.println(list);
        JsonResult jsonResult = new JsonResult(200,"查询成功",map);
        return jsonResult;
    }

    @RequestMapping("/max")
    @ResponseBody
    public JsonResult fo() {
        List list = comicService.max();
        Map<String,Object> map = new HashMap<>();
        map.put("fo",list);
        System.out.println(list);
        JsonResult jsonResult = new JsonResult(200,"查询成功",map);
        return jsonResult;
    }

    @RequestMapping("/maxthree")
    @ResponseBody
    public JsonResult ft() {
        List list = comicService.maxthree();
        Map<String,Object> map = new HashMap<>();
        map.put("fo",list);
        System.out.println(list);
        JsonResult jsonResult = new JsonResult(200,"查询成功",map);
        return jsonResult;
    }

    @RequestMapping("/iddetails/{sid}")
    @ResponseBody
    public JsonResult fq(@PathVariable("sid") int sid) {
        List list = comicService.iddetails(sid);

       if(list.size()==0){
           List id = comicService.idx(sid);
           Map<String,Object> map = new HashMap<>();
           map.put("fo",id);
           System.out.println(id);
           JsonResult jsonResult = new JsonResult(200,"查询查询成功",map);
           return jsonResult;
       }else{
           Map<String,Object> map = new HashMap<>();
           map.put("fo",list);
           System.out.println(list);
           JsonResult jsonResult = new JsonResult(200,"查询成功",map);
           return jsonResult;
       }
    }

    @RequestMapping("/idse/{sid}")
    @ResponseBody
    public JsonResult qq(@PathVariable("sid") int sid) {
        List list = sectionService.idse(sid);
            Map<String,Object> map = new HashMap<>();
            map.put("fo",list);
            System.out.println(list);
            JsonResult jsonResult = new JsonResult(200,"查询成功",map);
            return jsonResult;

    }

    //富文本上传图片
    @ResponseBody
    @RequestMapping("/wimg/upload")
    public Object Wimgupload(MultipartFile files){
        @Data
        class Res{
            int errno;
            Img data=new Img();

            @Data
            class Img{
                String url;
            }
        }
        List<String> array=ImgUpload.uploadMultipartFile(Collections.singletonList(files));
        Res res=new Res();
        for (String s:array) {
            res.errno=0;
            res.data.url=s;
        }
        System.out.println(res);
        return res;
    }

    //上传图片测试
    @ResponseBody
    @RequestMapping("/img/test")
    public String imgupload(MultipartFile files){
        System.out.println(files);
        List<String> array=ImgUpload.uploadMultipartFile(Collections.singletonList(files));
        System.out.println(array.get(0));
        return array.get(0);
    }

    //上传多张图片测试
    @ResponseBody
    @RequestMapping("/img/testlist")
    public List imguploadlist(MultipartFile files){
        System.out.println(files);
        List<String> array=ImgUpload.uploadMultipartFile(Collections.singletonList(files));
        for (int i=1;i<array.size();i++){
            System.out.println(array.get(i));
        }
        return array;
    }

//    添加章节
    @ResponseBody
    @RequestMapping("/addsect")
    public int add(@RequestBody Section section){
        System.out.println(section);
        int n = sectionService.adddata(section);
//    System.out.println(section);
//    List<String> array=ImgUpload.uploadMultipartFile(Collections.singletonList(files));
//    for (int i=1;i<array.size();i++){
//        System.out.println(array.get(i));
//    }
//    return array;
        return n;

}

//      根据id查询有多少章节
@ResponseBody
@RequestMapping("/idsection/{sid}")
public int chaxun(@PathVariable("sid") int sid) {
    int n = comicService.idse(sid);
    return n;
}

// 修改章节
    @ResponseBody
    @RequestMapping("/idzhang")
    public int zhang(@RequestBody Section section){
//        System.out.println(curPic);
        int n = sectionService.idzhang(section);
        System.out.println(section);
//        System.out.println(uid);
        return n;
    }

    //      根据id查询有多少章节
    @ResponseBody
    @RequestMapping("/iddel/{sid}")
    public int del(@PathVariable("sid") int sid) {
        int n = sectionService.delid(sid);
        return n;
    }
}

