package com.example.kuaikan.world.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.world.bean.World;
import com.example.kuaikan.world.mapper.WorldMapper;
import com.example.kuaikan.world.service.WorldService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author WxfZqs
 */
@Service
@Slf4j
public class WorldServiceImpl implements WorldService {
	@Resource
	WorldMapper worldMapper;
	
	@Override
	public boolean add(World world) {
		return 1 == worldMapper.insert(world);
	}
	
	@Override
	public boolean del(World world) {
		return 1 == worldMapper.deleteById(world.getId());
	}
	
	@Override
	public boolean update(World world) {
		return 1 == worldMapper.updateById(world);
	}
	
	@Override
	public World query(World world) {
		log.info("worldId: {}", world);
		return worldMapper.selectById(world.getId());
	}
	
	@Override
	public List<World> queryBatch(Integer page) {
		Page<World> size = new Page<>(page, 10);
		Page<World> uploadEntityPage = worldMapper.selectPage(size, null);
		return uploadEntityPage.getRecords();
	}

	@Override
	public List<World> queryauthor(World world) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("author",world.getAuthor());
		return worldMapper.selectList(queryWrapper);
	}

}
