package com.example.kuaikan.ranking.service.impl;

import com.example.kuaikan.entity.Section;
import com.example.kuaikan.ranking.dao.RankingDao;
import com.example.kuaikan.ranking.entity.Comics;
import com.example.kuaikan.ranking.service.RankingService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingjing
 */
@Service
public class RankingServiceImpl implements RankingService {

    @Resource
    RankingDao rankingDao;

    @Override
    public List listComic() {
        List item = rankingDao.listComic();
        List up = rankingDao.listSection();
        List list = new ArrayList();
        List list2 = new ArrayList();
        while (list.size() < 14){
            for(int i=0; i<item.size();i++){
                Comics cc = (Comics)item.get(i);
                Map map = new HashMap();
                map.put("id",cc.getId());
                map.put("labels", new String[]{cc.getTag()});
                map.put("img",cc.getImg_cover());
                map.put("name", cc.getName());
                map.put("author",cc.getUsername());
                map.put("description",cc.getDetails());
                map.put("recommend",cc.getRecommend());
                map.put("popularity",cc.getPopularity());
                map.put("update", "");
                up.forEach(v -> {
                    Section se = (Section)v;
                    if(se.getComic_id()==cc.getId()){
                        map.put("update", se.getNickname());
                    }
                });

                list2.add(map);
                if(list2.size()==10){
                    list.add(list2);
                    list2 = new ArrayList();
                }
            }
        }
        return list;
    }
}
