package com.example.kuaikan.classify.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tag {
//    标签名
    String tag;
//    判断是否被选中
    boolean isfs;
}
