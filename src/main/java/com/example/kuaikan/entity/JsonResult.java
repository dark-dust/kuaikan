package com.example.kuaikan.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class JsonResult {
    int code;
    String msg;
    Map<String,Object> map=new HashMap<>();

    public JsonResult() {
    }


    public JsonResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "JsonResult{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", map=" + map +
                '}';
    }

    public JsonResult(int code, String msg, Map<String, Object> map) {
        this.code = code;
        this.msg = msg;
        this.map = map;
    }
}
