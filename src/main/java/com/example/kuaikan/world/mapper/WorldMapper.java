package com.example.kuaikan.world.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.kuaikan.world.bean.World;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WorldMapper extends BaseMapper<World> {
}
