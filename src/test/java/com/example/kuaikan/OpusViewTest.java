package com.example.kuaikan;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.classify.dao.OpusViewDao;
import com.example.kuaikan.classify.entity.Comic2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class OpusViewTest {
    @Autowired
    OpusViewDao opusViewDao;


    @Test
    void test(){
        List count = opusViewDao.getComicCount(853);
        System.out.println(count);
    }

    @Test
    void test2(){
        List list = opusViewDao.getCommentCount(853);
        System.out.println(list);
    }

    @Test
    void test3(){
        List list = opusViewDao.getReplyCount(853);
        System.out.println(list);
    }
}
