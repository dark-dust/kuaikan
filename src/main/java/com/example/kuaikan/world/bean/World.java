package com.example.kuaikan.world.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author WxfZqs
 */
@Data
@TableName("world")
public class World {
	@TableId
	int id;
	int author;
	String content;
	String title;
	String image;
	String praise;
}
