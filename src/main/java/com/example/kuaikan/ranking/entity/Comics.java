package com.example.kuaikan.ranking.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lingjing
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comics {
    int id;
    /**漫画名字*/
    String name;
    /**漫画详情*/
    String details;
    /**漫画封面*/
    String img_cover;
    /**推荐数量*/
    int recommend;
    /**人气值*/
    int popularity;
    /**标签*/
    String tag;
    /**作者名*/
    String username;

    @Override
    public String toString() {
        return "\nComic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", details='" + details + '\'' +
                ", img_cover='" + img_cover + '\'' +
                ", recommend=" + recommend +
                ", popularity=" + popularity +
                ", tag='" + tag + '\'' +
                ", username='" + username + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getImg_cover() {
        return img_cover;
    }

    public void setImg_cover(String img_cover) {
        this.img_cover = img_cover;
    }

    public int getRecommend() {
        return recommend;
    }

    public void setRecommend(int recommend) {
        this.recommend = recommend;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
