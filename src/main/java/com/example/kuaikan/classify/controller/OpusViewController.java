package com.example.kuaikan.classify.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.classify.dao.ClassifyDao;
import com.example.kuaikan.classify.dao.OpusViewDao;
import com.example.kuaikan.classify.entity.Comic2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class OpusViewController {
    @Autowired
    OpusViewDao opusViewDao;
    //    评论数据
    @GetMapping("/CommentCount")
    public @ResponseBody
    List<Map> comicTougao(ModelMap map,  int userid) {
        List list = opusViewDao.getCommentCount( userid);
        List<Map> listMap = new ArrayList<Map>();
        listMap.add(map);
        map.put("listCom", list);
        return listMap;
    }
    //    回复数据
    @GetMapping("/ReplyCount")
    public @ResponseBody
    List<Map> comicZj(ModelMap map,  int userid) {
        List list = opusViewDao.getReplyCount( userid);
        List<Map> listMap = new ArrayList<Map>();
        listMap.add(map);
        map.put("listRep", list);
        return listMap;
    }
}
