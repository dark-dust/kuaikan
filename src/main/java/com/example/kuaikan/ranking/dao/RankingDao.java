package com.example.kuaikan.ranking.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author lingjing
 */
@Mapper
public interface RankingDao {

    /**
     * 查询所有漫画
     * @return
     */
    List listComic();

    /**
     * 查询所有漫画最后一章
     * @return
     */
    List listSection();
}
