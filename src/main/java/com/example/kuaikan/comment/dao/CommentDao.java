package com.example.kuaikan.comment.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.comment.entity.Comment;
import com.example.kuaikan.comment.entity.Reply;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface CommentDao {
// 查询评论
  List getComment(Page<Comment> page,int worldid);
//查询回复
  List getReply();
//添加评论
  int getaddComment(String text,int userid,int worldid);
//添加回复
  int getaddReply(String text,int userid,int worldid);
//查询点赞
   List getLikeComment(int userid,int worldid);
//修改点赞数据
   int getUpdLikeCom(int id,int like,String useridLike);

}
