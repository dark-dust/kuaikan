package com.example.kuaikan.classify.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SearchDao {
    List getSearch(String name);
    List getSeaUName(String username);

}
