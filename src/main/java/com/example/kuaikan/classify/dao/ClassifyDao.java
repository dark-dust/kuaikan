package com.example.kuaikan.classify.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.classify.entity.Comic2;
import com.example.kuaikan.entity.Comic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ClassifyDao {
//  查询分类表
    //取出漫画
     List getComic(Page<Comic2> page);
    //根据标签查询漫画
     List getBqComic(Page<Comic2> page,String tag);
    //根据漫画表查询所有标签
     List getComicTag();
    //根据漫画热度查询漫画
     List getComicHot(Page<Comic2> page,String tag);
    //后台根据用户查询漫画
    List getComicUser(Page<Comic2> page,int author);
    //后台投稿查询漫画
    List getComicTouGao(int author);
    //投稿漫画章节数据
    List getComicSection(int id);
    //投稿搜索
    List getTouGaoSearch(int author,String name);
    //投稿删除
    int getDelTouGao(int id);
    //人气值增加
    int getUpdDetails(int popularity,int id);
}
