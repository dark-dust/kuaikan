package com.example.kuaikan.dao;

import com.example.kuaikan.entity.Section;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SectionDao {
    List add();
    int adddata(Section ob);
    //    根据id查询详情页
    List idse(@Param("uid") int uid);

//    修改章节
    int idzhang(Section section);

//    删除章节
    int delid(@Param("uid") int uid);

}
