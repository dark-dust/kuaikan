package com.example.kuaikan.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.kuaikan.user.bean.User;
import com.example.kuaikan.user.mapper.UserMapper;
import com.example.kuaikan.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Resource
    UserMapper UserMapper;

    @Override
    public boolean add(User user) {
        return 1==UserMapper.insert(user);
    }

    @Override
    public boolean del(User user) {
        return 1==UserMapper.deleteById(user);
    }

    @Override
    public boolean update(User user) {
        return 1==UserMapper.updateById(user);
    }

    @Override
    public User query(User user) {
        return UserMapper.selectById(user.getUserid());
    }

    @Override
    public User login(User user){
        QueryWrapper wrapper=new QueryWrapper<>();
        wrapper.eq("email",user.getEmail());
        wrapper.eq("userpassword",user.getUserpassword());
        return UserMapper.selectOne(wrapper);
    }
}
