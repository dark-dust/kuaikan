package com.example.kuaikan.dao;

import com.example.kuaikan.entity.Comic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface ComicDao {
//    查询漫画表
    List query();

    boolean addquery(Comic ob);
    int delquery(@Param("uid") int uid);
//    随机查5条数据
    List five();
//    随机查10条数据
    List ten();
//    查询热度最高的10条
    List max();
//    查询前三条
    List maxthree();
//    根据id查询详情页
  List iddetails(@Param("uid") int uid);

//  id查询详情
    List idx(@Param("uid") int uid);

//    根据id查询有多少条章节数据
    int idse(@Param("uid") int uid);
}
