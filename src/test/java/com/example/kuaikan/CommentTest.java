package com.example.kuaikan;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.comment.entity.Comment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.example.kuaikan.comment.dao.CommentDao;
import java.util.List;

@SpringBootTest
public class CommentTest {
    @Autowired
    CommentDao commentDao;
    @Test
    void test(){
        Page<Comment>   page = new Page<>(1, 5);
        List pageResult = commentDao.getComment(page,5);
        System.out.println(pageResult);
    }
    @Test
    void test2(){
        List pageResult = commentDao.getReply();
        System.out.println(pageResult);
    }
    //    添加评论
    @Test
    void test3(){
        int add = commentDao.getaddComment("springBoot测试添加",787,5);
        System.out.println(add);
    }
    //    添加回复
    @Test
    void test4(){
        int add = commentDao.getaddReply("springBoot测试添加",787,22);
        System.out.println(add);
    }
    //    判断点赞
    @Test
    void test5(){
        List list = commentDao.getLikeComment(777,5);
        System.out.println(list);
    }
    //    修改点赞
    @Test
    void test6(){
        int add = commentDao.getUpdLikeCom(2,3,"780,781,782");
        System.out.println(add);
    }
}

