package com.example.kuaikan.entity;

import lombok.Data;

import java.util.List;

@Data
public class Section {
    int comic_id;    //漫画id
    String curPic;  //章节内容
    int location;   //章节位置
    String nickname;    //章节名称
    int uid;         //章节id
    public Section(){


    }

    @Override
    public String toString() {
        return "Section{" +
                "comic_id=" + comic_id +
                ", curPic='" + curPic + '\'' +
                ", location=" + location +
                ", nickname='" + nickname + '\'' +
                ", uid=" + uid +
                '}';
    }

    public int getComic_id() {
        return comic_id;
    }

    public void setComic_id(int comic_id) {
        this.comic_id = comic_id;
    }

    public String getCurPic() {
        return curPic;
    }

    public void setCurPic(String curPic) {
        this.curPic = curPic;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Section(int comic_id, String curPic, int location, String nickname, int uid) {
        this.comic_id = comic_id;
        this.curPic = curPic;
        this.location = location;
        this.nickname = nickname;
        this.uid = uid;
    }
}
