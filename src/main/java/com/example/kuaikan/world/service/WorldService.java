package com.example.kuaikan.world.service;

import com.example.kuaikan.world.bean.World;

import java.util.List;

/**
 * @author WxfZqs
 */
public interface WorldService {
	/**
	 * @param world 世界实体
	 * @description 添加一个世界内容
	 * */
	boolean add(World world);
	/**
	 * @param world 世界实体
	 * @description 删除一个世界内容
	 * */
	boolean del(World world);
	/**
	 * @param world 世界实体
	 * @description 修改某个世界的属性
	 * */
	boolean update(World world);
	/**
	 * @param world 世界对象
	 * @description 根据Id查询World内容
	 * */
	World query(World world);
	/**
	 * @param page 第几页
	 * @description 查询第几页World
	 * */
	List<World> queryBatch(Integer page);
	/*
	* 用author查询数据
	* */
	List<World> queryauthor(World world);
}
