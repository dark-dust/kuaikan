package com.example.kuaikan.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
public class Comic {
    int id;
    String name;  //漫画名字
    String details; //漫画详情
    String img_cover; //漫画封面
    int recommend; //推荐数量
    int author;//作者ID
    int popularity; //人气值
    String tag; //标签
    List<Section> sectiong;//一对多映射




    @Override
    public String toString() {
        return "Comic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", details='" + details + '\'' +
                ", img_cover='" + img_cover + '\'' +
                ", recommend=" + recommend +
                ", author=" + author +
                ", popularity=" + popularity +
                ", tag='" + tag + '\'' +
                ", sectiong=" + sectiong +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getImg_cover() {
        return img_cover;
    }

    public void setImg_cover(String img_cover) {
        this.img_cover = img_cover;
    }

    public int getRecommend() {
        return recommend;
    }

    public void setRecommend(int recommend) {
        this.recommend = recommend;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<Section> getSectiong() {
        return sectiong;
    }

    public void setSectiong(List<Section> sectiong) {
        this.sectiong = sectiong;
    }



    public Comic(int id, String name, String details, String img_cover, int recommend, int author, int popularity, String tag) {
        this.id = id;
        this.name = name;
        this.details = details;
        this.img_cover = img_cover;
        this.recommend = recommend;
        this.author = author;
        this.popularity = popularity;
        this.tag = tag;
    }
}
