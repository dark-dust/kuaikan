package com.example.kuaikan.user.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 用户类
 */
@Data
@TableName("user")
public class User {
    @TableId
    int userid; //用户id
    String username; //用户名字
    String userpassword; //用户密码
    String email; //用户邮箱
    String mobile; //用户手机号
    String headportrait; //用户头像
}
