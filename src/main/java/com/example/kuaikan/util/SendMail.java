package com.example.kuaikan.util;

import org.apache.commons.mail.SimpleEmail;

import java.util.Random;

/**
 * @program: BlogGarden
 * @description: 发送邮件类
 * @author: 李贝林
 * @create: 2021-10-06 08:34
 */
public class SendMail {
    /**
     * @param to    收件人
     * @param vcode 邮件内容
     * @Description: 发送邮件
     * @Param:
     * @return: null
     * @Author: 李贝林
     * @date: 2021/10/8
     */
    public static void sendMail(String to, String vcode) throws Exception {
        SimpleEmail simpleEmail = new SimpleEmail();
        //开启SSL加密
        simpleEmail.setSSLOnConnect(false);
        //SMTP服务器的端口
        simpleEmail.setSslSmtpPort("465");
        //SMTP服务器的名字
        simpleEmail.setHostName("smtp.qq.com");
        //发件人邮箱以及授权码
        simpleEmail.setAuthentication("2959032098@qq.com", "cdbvaipghpjjdeai");
        //编码集
        simpleEmail.setCharset("UTF-8");
        //收件人邮箱
        simpleEmail.addTo(to);
        //发件人邮箱以及发件人名称
        simpleEmail.setFrom("2959032098@qq.com", "快看漫画:");
        //邮件主题
        simpleEmail.setSubject("快看验证码");
        //邮件内容
        simpleEmail.setMsg(vcode);
        simpleEmail.send();
    }

    public static String getVCode() {
        StringBuilder vCode = new StringBuilder();
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        for (int i = 0; i < 6; i++) {
            vCode.append((int)(random.nextDouble() * 9));
        }
        return vCode.toString();

    }
}
