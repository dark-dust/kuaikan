package com.example.kuaikan.ranking.controller;

import com.example.kuaikan.ranking.service.RankingService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lingjing
 */
@RestController
public class RankingController {

    @Resource
    RankingService rankingService;

    /**
     * 14次随机查询10部漫画
     */
    @RequestMapping(path = "/listranking",method = RequestMethod.GET)
    public List listRanking(){
        return rankingService.listComic();
    }

}
