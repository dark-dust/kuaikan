package com.example.kuaikan.service;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.File;


@Service
public interface UploadService {
    /**
     * 上传文件
     * @param file File
     * @return
     * @throws QiniuException
     */
    Response uploadFile(File file) throws QiniuException;
}