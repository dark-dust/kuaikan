package com.example.kuaikan;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.classify.dao.ClassifyDao;
import com.example.kuaikan.classify.entity.Comic2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class ClassifyTest {
    @Autowired
    ClassifyDao classifyDao;


    @Test
    void test(){
        String pg="2";
        Page<Comic2>   page = new Page<>( Integer.parseInt(pg), 12);
        List pageResult = classifyDao.getComic(page);
        System.out.println(pageResult);
    }
//    标签查询漫画
    @Test
    void test2(){
        Page<Comic2>   page = new Page<>( 1, 12);
        List list=classifyDao.getBqComic(page,"热血");
        System.out.println(list);
    }
//    热度查询
    @Test
    void test3(){
        Page<Comic2>   page = new Page<>( 1, 12);
        List list=classifyDao.getComicHot(page,"热血");
        System.out.println(list);
    }
//    查询标签
    @Test
    void test4(){
        List list=classifyDao.getComicTag();
        System.out.println(list);
    }
    //    查询标签
    @Test
    void test5(){
        List list=classifyDao.getComicTouGao(813);
        System.out.println(list);
    }
    @Test
    void test6(){
        List list=classifyDao.getComicSection(221);
        System.out.println(list);
    }

    @Test
    void test7(){
        List list=classifyDao.getTouGaoSearch(853,"萝莉");
        System.out.println(list);
    }
    @Test
    void test8(){
        int num=classifyDao.getDelTouGao(305);
        System.out.println(num);
    }
    @Test
    void test10(){
        int num=classifyDao.getUpdDetails(4702,221);
        System.out.println(num);
    }
}
