package com.example.kuaikan.util;

import com.google.gson.Gson;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * @program: BlogGarden
 * @description:图片上传通用类
 * @author: 李贝林
 * @create: 2021-11-16 15:58
 */
public class ImgUpload {
    //构造一个带指定 Region 对象的配置类
    static Configuration cfg = new Configuration(Region.huanan());
    //...其他参数参考类注释
    static UploadManager uploadManager = new UploadManager(cfg);
    //...生成上传凭证，然后准备上传
    static String accessKey = "OknRdPoJxGj5MBBv4aghuwLvTa0m-lOTKwNUR6Uw";
    static String secretKey = "_X52l0Pw2pEX1L_sM_gQOrUq8JuxyrQQ4sWKr0xN";
    static String bucket = "springbootkuaikan2";

    static Auth auth = Auth.create(accessKey, secretKey);
    static String upToken = auth.uploadToken(bucket);

    /**
    *@Description: 上传多张图片
    *@Param: files file对象集合
    *@return: 返回图片链接(可通过互联网访问)
    *@Author: 李贝林
    *@date: 2021/11/16
    */
    public static List<String> uploadController(List<File> files){
        List<String> array = new ArrayList();
        //文件保存
        for(File file :files) {
            DefaultPutRet putRet = null;
            try {
                Response response = uploadManager.put(file, null, upToken);
                //解析上传成功的结果
                putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
               array.add("http://rcxtugtwe.hn-bkt.clouddn.com/" + putRet.key);//将图片的引用url返回

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return array;
    }

    public static List<String> uploadMultipartFile(List<MultipartFile> files) {
        List<String> array = new ArrayList();
        //文件保存
        for(MultipartFile file :files) {
            DefaultPutRet putRet = null;
            try {
                Response response = uploadManager.put(file.getBytes(), null, upToken);
                //解析上传成功的结果
                putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);

                array.add("http://rcxtugtwe.hn-bkt.clouddn.com/" + putRet.key);//将图片的引用url返回
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return array;
    }
}
