package com.example.kuaikan.comment.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.comment.dao.CommentDao;
import com.example.kuaikan.comment.entity.Comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class CommentController {
    @Autowired
    CommentDao commentDao;

//    查询评论
    @GetMapping("/Comment")
    public @ResponseBody List<Map> comment(ModelMap map,String size,String wordid) {
        List<Map> listMap = new ArrayList<Map>();
        Page<Comment> page = new Page<>( 1, Integer.parseInt(size));
        //评论数据
        List list = commentDao.getComment(page,Integer.parseInt(wordid));

        System.out.println(list);
        map.put("list", list);
        map.put("p", page);
        listMap.add(map);
        return listMap;
    }
//    查询回复
    @GetMapping("/Reply")
    public @ResponseBody List<Map> reply(ModelMap map,String size) {
        List<Map> listMap = new ArrayList<Map>();
        //回复数据
        List list = commentDao.getReply();
        System.out.println(list);
        map.put("list", list);
        listMap.add(map);
        return listMap;
    }
//    添加评论
    @GetMapping("/addComment")
    public @ResponseBody boolean addComment(String text,int userid,int worldid) {
        int add=commentDao.getaddComment(text,userid,worldid);
        System.out.println(add);
        boolean dc;
        dc= add != 0;
        return dc;
    }
//    添加回复
    @GetMapping("/addReply")
    public @ResponseBody boolean addReply(String text,int userid,int worldid) {
        int add=commentDao.getaddReply(text,userid,worldid);
        System.out.println(add);
        boolean dc;
        dc= add != 0;
        return dc;
    }
    @GetMapping("/LikeComment")
    public @ResponseBody List<Map> reply(ModelMap map,int worldid,int userldid) {
        List<Map> listMap = new ArrayList<Map>();
        //回复数据
        List list = commentDao.getLikeComment(userldid,worldid);
        System.out.println(list);
        map.put("listLike", list);
        listMap.add(map);
        return listMap;
    }
    @GetMapping("/updLikeCom")
    public @ResponseBody boolean updLikeCom(int id,int like,String useridLike) {
        System.out.println("userldLike"+useridLike);
        int add=commentDao.getUpdLikeCom(id,like,useridLike);
        System.out.println(add);
        boolean dc;
        dc= add != 0;
        return dc;
    }
}
