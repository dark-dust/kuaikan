package com.example.kuaikan.classify.dao;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.classify.entity.Comic2;
import com.example.kuaikan.entity.Comic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface OpusViewDao {
//    作者漫画数据
    List getComicCount(int author);
    List getCommentCount(int userid);
    List getReplyCount(int userid);

}
