package com.example.kuaikan.user.service;

import com.example.kuaikan.user.bean.User;

/**
 * 用户Service
 * @author 李贝林
 */
public interface UserService {
    /**
     * 增加用户
     * @param user 新增用户对象
     * @return
     */
    boolean add(User user);

    /**
     * 删除用户
     * @param user 用户对象
     * @return
     */
    boolean del(User user);

    /**
     * 修改用户数据
     * @param user 用户对象
     * @return
     */
    boolean update(User user);

    /**
     * 根据id查询用户
     * @param user 用户对象
     * @return
     */
    User query(User user);

    /**
     * 根据用户名和密码登录查询一个用户
     * @param user
     * @return
     */
    User login(User user);
}
