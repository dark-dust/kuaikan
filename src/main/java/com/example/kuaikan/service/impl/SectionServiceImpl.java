package com.example.kuaikan.service.impl;


import com.example.kuaikan.dao.SectionDao;
import com.example.kuaikan.entity.Section;
import com.example.kuaikan.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectionServiceImpl implements SectionService {
    @Autowired
    SectionDao sectionDao;
    @Override
    public int adddata(Section ob) {
        System.out.println(ob);
        int n =sectionDao.adddata(ob);
        return n;
    }

    @Override
    public List idse(int uid) {
        List obj = sectionDao.idse(uid);
        return obj;
    }

    @Override
    public int idzhang(Section section) {
        int n = sectionDao.idzhang(section);
        return n;
    }

    @Override
    public int delid(int uid) {
        int n = sectionDao.delid(uid);
        return n;
    }
}
