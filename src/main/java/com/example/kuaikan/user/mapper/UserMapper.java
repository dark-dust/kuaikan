package com.example.kuaikan.user.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.kuaikan.user.bean.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
