package com.example.kuaikan.classify.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comic2 {
    int id;
    String name;        //漫画名字
    String details;     //漫画详情
    String img_cover;   //漫画封面
    int recomment;      //推荐数量
    int author;         //作者id
    int popularity;     //人气值
    String tag;         //标签
//    多表联查  user表作者名
    String username;
    int location;
    String nickname;
    @Override
    public String toString() {
        return "\nComic2{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", details='" + details + '\'' +
                ", img_cover='" + img_cover + '\'' +
                ", recomment=" + recomment +
                ", author=" + author +
                ", popularity=" + popularity +
                ", tag='" + tag + '\'' +
                ", username='" + username + '\'' +
                ", location=" + location +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
