package com.example.kuaikan.world.controller;

import com.example.kuaikan.world.bean.World;
import com.example.kuaikan.world.service.WorldService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author WxfZqs
 */
@RequestMapping("/world")
@RestController
public class WorldController {
	@Resource
	WorldService worldService;
	
	@PostMapping("/add")
	public boolean add(@RequestBody World world) {
		return worldService.add(world);
	}
	
	@PostMapping("/del")
	public boolean del(@RequestBody World world) {
		return worldService.del(world);
	}
	
	@PostMapping("/update")
	public boolean update(@RequestBody World world) {
		return worldService.update(world);
	}
	
	@GetMapping("/query")
	public World query(World world) {
		return worldService.query(world);
	}
	
	@GetMapping("/queryBatch")
	public List<World> queryBatch(@RequestParam(value = "page", defaultValue = "0") Integer page) {
		System.out.println(page);
		return worldService.queryBatch(page);
	}
	@GetMapping("/queryauthor")
	public List<World> queryauthor(World world){
		System.out.println(world);
		return worldService.queryauthor(world);
	}
}
