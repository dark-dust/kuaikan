package com.example.kuaikan.comment.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    int id;
    String text;
    int userid;                     //用户id
    int worldid;                    //世界id
    Date date;                      //时间
    int like;                       //点赞数
    String useridLike;               //点赞用户id
    String username;                //用户名
    String headportrait;            //头像
    boolean isfs;                   //判断回复
    boolean likefs;                 //判断点赞
    String[] commentwordid;         //放回复数据的地方

    @Override
    public String toString() {
        return "\nComment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", userid=" + userid +
                ", worldid=" + worldid +
                ", date=" + date +
                ", like=" + like +
                ", useridLike=" + useridLike +
                ", username='" + username + '\'' +
                ", headportrait='" + headportrait + '\'' +
                ", isFs=" + isfs +
                ", Likefs=" + likefs +
                ", commentwordid=" + Arrays.toString(commentwordid) +
                '}';
    }
}
