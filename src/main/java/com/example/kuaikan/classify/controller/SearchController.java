package com.example.kuaikan.classify.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.kuaikan.classify.dao.SearchDao;
import com.example.kuaikan.classify.entity.Comic2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class SearchController {
    @Autowired
    SearchDao searchDao;
    @GetMapping("/SearchNa")
    public @ResponseBody
    List<Map> comic(ModelMap map, String name) {
//        漫画书名查询的漫画数据
        List list = searchDao.getSearch(name);
//        作者名查询的漫画数据
        List ListUName = searchDao.getSeaUName(name);
        System.out.println("list"+list);
        System.out.println("ListUName"+ListUName);
        List<Map> listMap = new ArrayList<Map>();
        map.put("list", list);
        map.put("listu",ListUName);
        listMap.add(map);
        return listMap;
    }
}
